---
aliases:
  - ../../plasma-5.22.90
authors:
  - SPDX-FileCopyrightText: 2021 Nate Graham <nate@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2021 Aniqa Khokhar <aniqa.khokhar@kde.org
  - SPDX-FileCopyrightText: 2021 Björn Feber <bfeber@protonmail.com>
  - SPDX-FileCopyrightText: 2021 Guilherme Marçal Silva <guimarcalsilva@gmail.com>
SPDX-License-Identifier: CC-BY-4.0
changelog: 5.22.5-5.22.90
date: 2021-09-16
layout: plasma-5.23
title: 'Plasma 25th Anniversary Edition Beta'
draft: false
sassFiles:
  - /sass/plasma-5-22.scss
---

{{< container >}}

{{% i18n_date %}}

This is the Beta release of Plasma - 25th Anniversary Edition. To make sure that end-users have the best possible experience with the next version of Plasma, KDE is releasing today this test version of the software. We encourage the more adventurous to test-run it and report problems so that developers may iron out the wrinkles before the final release scheduled for the 12th of October.

Plasma 25th Anniversary Edition is a leap forward in the quest for a more performant and usable desktop. We have improved the speed and stability of Plasma, while including changes that make it easier to use both on desktop and touch-enabled devices.

**DISCLAIMER:** This is beta software and is released for testing purposes. You are advised to NOT use Plasma 25th Anniversary Edition Beta in a production environment or as your daily desktop. If you do install Plasma 25th Anniversary Edition Beta, you must be prepared to encounter (and [report to the creators](https://bugs.kde.org/)) bugs that may interfere with your day-to-day use of your computer.

These are the most noteworthy changes coming in Plasma 25th Anniversary Edition Beta:

## Breeze

{{< feature src="breeze-application-style.png" class="round-top-right" >}}

We made several improvements to the Breeze theme to make Plasma even more pleasant to use:

- Breeze now has redesigned buttons, menu items, checkboxes, radio buttons, sliders, scrollbar and spinboxes
- Scrollbars and spinboxes are bigger and easier to use with touch
- We added a gear loading spinner throughout Plasma and KDE apps
- There is a highlight effect for when opened widgets touch the panel edge
- Widgets on the desktop now have a blurred background

{{< /feature >}}

{{< /container >}}

{{< diagonal-box color="purple" >}}

## Kickoff

![System monitor logo](kickoff.png)
Kickoff, our application launcher, includes many improvements this time around:

- The code received a gigantic overhaul
- A huge number of bugs were fixed
- It has improved performance and accessibility
- You can now choose whether to use a list or a grid for all apps view
- Now you can make Kickoff stay on the screen with the new pin button
- Pressing-and-holding with a finger now opens the context menu
- You can now configure power/session action buttons

{{< /diagonal-box >}}

{{< container >}}

## Other Widgets

{{< figure src="plasma-nm.png" alt="details about the currently-connected network" style="float: right" >}}

- Entering tablet mode makes System Tray icons bigger, making it easier to use Plasma on touch screens
- You are now able to easily copy text on notifications with the Ctrl+C keyboard shortcut
- The Global Menu applet got a more "menu-like" appearance
- We added an easy way to switch power profiles. You can choose between "power-saver", "balanced" and "performance"
- It is now possible to fine-tune the manual speed setting for wired Ethernet connections and to disable IPv6
- Plasma now shows more details about the currently-connected network
- System Monitor and sensor widgets can now display load averages
- By default, the clipboard now remembers 20 items and ignores selections that you did not explicitly copy
- The Audio Volume applet now distinguishes between applications that are currently playing or recording audio
- It is now possible to delete selected items in the Clipboard applet’s popup by pressing the Delete key on the keyboard

## System Settings

![System Settings](systemsettings.svg)

System Settings also received many new additions and improvements:

- Now you can override a color scheme's accent color in the Colors page
- The Feedback page now shows you a history of data that you chose to send to KDE
- You can set the status of your Bluetooth adapter on login. The options available are "on", "off" and "remember"
- Login Screen page’s synchronization feature now syncs your screen arrangement
- Display settings now shows a timer to revert possibly undesired changes
- We sorted the screen refresh rates list
- Night Color page now tells you when you’re about to undertake an action that will perform geolocation using a 3rd-party service
- Now it is easier to find options in System Settings. We added many keywords to improve search.

{{< figure src="system-settings-accent-colour.png" alt="override a color scheme's accent color" >}}

# Discover

![Discover](plasmadiscover.svg)
Discover, our app center, is faster and more transparent in this version:

- Discover will now load faster
- The app install button now tells you the name of the source

{{< /container >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## Wayland

In this release we made many improvements to the Wayland session:

- There is a new screen rotation animation
- The System Tray will notify you when something is recording the screen and will also let you cancel it
- Using touchpad gestures will be more satisfying than ever. Now the animation of realtime gestures follows your fingers
- It is now possible to middle-click-paste between native Wayland and XWayland apps
- You can now drag-and-drop stuff between native Wayland and XWayland apps
- In the Plasma Wayland session, it is now possible to adjust the Intel GPU driver’s Broadcast RGB settings
- It is now possible to change the screen resolution when run in a virtual machine
- Virtual desktops are now remembered on a per-activity basis
- The Task Manager now shows feedback on app icons when you click on them to launch apps
- The cursor now shows animated icon feedback when launching apps

{{< /diagonal-box >}}

{{< container >}}

## Other

{{< figure src="media-player-applet.png" alt="Media Player Applet" style="float: right" >}}

- The "Present Windows" effect was rewritten
- Multi-screen layouts are now retained across X11 and Wayland sessions
- Numerous crash/hangs have been fixed
- DrKonqi, our bug reporting application, warns you when an app is unmaintained
- The question mark button in the titlebar is hidden by default for dialogs and System Settings
- Windows no longer become transparent when moving or resizing them
- Passwordless accounts without autologin now show a simple login button
- The Media Player widget now always displays the album art and its blurred background at the same time
- The Networks applet now supports additional authentication settings/protocols/requirements for OpenVPN connections

If you would like to learn more, [check out the full changelog for Plasma 5.23 Beta](/announcements/changelogs/plasma/5/5.22.5-5.22.90).

{{< /container >}}
