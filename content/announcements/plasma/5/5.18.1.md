---
aliases:
- ../../plasma-5.18.1
changelog: 5.18.0-5.18.1
date: 2020-02-18
layout: plasma
peertube: cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
asBugfix: true
---

- <a href="https://bugs.kde.org/show_bug.cgi?id=415155">Closing window from Present Windows effect sometimes causes the new frontmost window to lose focus and be unable to regain it</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=417424">On upgrade to 5.18, a desktop with locked widgets remains locked, and cannot be unlocked.</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=416358">kcm_fonts: Cannot apply changes (button remains inactive)</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=416695">Chord keyboard shortcuts that begin with Alt+D all open the desktop settings window</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=413915">All electron apps have grey menus when using breeze gtk3 theme</a>
