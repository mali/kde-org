---
aliases:
- ../../plasma-5.6.2
changelog: 5.6.1-5.6.2
date: 2016-04-05
layout: plasma
youtube: v0TzoXhAbxg
figure:
  src: /announcements/plasma/5/5.6.0/plasma-5.6.png
  class: mt-4 text-center
asBugfix: true
---

- Weather plasmoid, bbcukmet. Update to BBC's new json-based search and modified xml. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=84fe5785bd1520f17a801cfe2e263c8ba872b273">Commit.</a> Fixes bug <a href="https://bugs.kde.org/330773">#330773</a>
- Breeze and Oxygen widget themes: Add isQtQuickControl function and make it work with Qt 5.7. <a href="http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f59ae8992d18718d596fd332389b3fe98ff21a10">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127533">#127533</a>
- [calendar] Fix calendar applet not clearing selection when hiding. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3beb0b647a543436d3d23ab82b39a2f98a384be">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127456">#127456</a>. Fixes bug <a href="https://bugs.kde.org/360683">#360683</a>
