---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE Ships KDE Applications 16.08.1
layout: application
title: KDE Ships KDE Applications 16.08.1
version: 16.08.1
---

{{% i18n_var "September 8, 2016. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.08.0" %}}

More than 45 recorded bugfixes include improvements to kdepim, kate, kdenlive, konsole, marble, kajongg, kopete, umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.24" %}}