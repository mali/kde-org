---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE Ships KDE Applications 17.04.3
layout: application
title: KDE Ships KDE Applications 17.04.3
version: 17.04.3
---

{{% i18n_var "July 13, 2017. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.04.0" %}}

More than 25 recorded bugfixes include improvements to kdepim, dolphin, dragonplayer, kdenlive, umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.34" %}}