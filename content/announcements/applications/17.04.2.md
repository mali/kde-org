---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE Ships KDE Applications 17.04.2
layout: application
title: KDE Ships KDE Applications 17.04.2
version: 17.04.2
---

{{% i18n_var "June 8, 2017. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.04.0" %}}

More than 15 recorded bugfixes include improvements to kdepim, ark, dolphin, gwenview, kdenlive, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.33" %}}