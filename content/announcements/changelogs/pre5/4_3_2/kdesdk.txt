------------------------------------------------------------------------
r1017642 | scripty | 2009-08-31 03:00:55 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019172 | scripty | 2009-09-03 04:09:56 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019506 | lueck | 2009-09-03 18:53:18 +0000 (Thu, 03 Sep 2009) | 1 line

sdk doc backport for 4.3
------------------------------------------------------------------------
r1019522 | lueck | 2009-09-03 19:43:58 +0000 (Thu, 03 Sep 2009) | 1 line

fogot this too sdk doc backport for 4.3
------------------------------------------------------------------------
r1020019 | scripty | 2009-09-05 03:20:40 +0000 (Sat, 05 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024440 | scripty | 2009-09-16 16:12:29 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025880 | markuss | 2009-09-19 23:32:43 +0000 (Sat, 19 Sep 2009) | 1 line

Fix a missing icon
------------------------------------------------------------------------
r1025967 | markuss | 2009-09-20 09:48:07 +0000 (Sun, 20 Sep 2009) | 1 line

Fix a missing icon
------------------------------------------------------------------------
r1028716 | kkofler | 2009-09-28 00:20:03 +0000 (Mon, 28 Sep 2009) | 6 lines

Rectify the antialiasing changes that were put in a while back

The 1px border lines for selected items are now drawn down one line of pixels instead of being drawn on the boundary between two lines.
I've also changed the connectwidget bezier rendering stuff to use QPainterPath beziers. Before antialiasing was introduced the Q3PointArray bezier-approximations were fine, but with antialiasing they were noticably incorrect. Qt4's QPainterPath gets it right.

backport revision 1017555 by je4d from trunk
------------------------------------------------------------------------
r1028717 | kkofler | 2009-09-28 00:21:24 +0000 (Mon, 28 Sep 2009) | 3 lines

Backport r1021024 from 3_way_kompare - a bugfix for an issue with the antialiased rendering I previouly ported over from 3_way_kompare (first pixel line of connectwidget was 50% garbage)

backport revision 1021025 by je4d from trunk
------------------------------------------------------------------------
r1028718 | kkofler | 2009-09-28 00:23:59 +0000 (Mon, 28 Sep 2009) | 3 lines

backport correct tab-to-space expansion from 3_way_kompare r1026025

backport revision 1026032 by je4d from trunk
------------------------------------------------------------------------
r1028719 | kkofler | 2009-09-28 00:26:59 +0000 (Mon, 28 Sep 2009) | 5 lines

Revert the fix for bug 139209 - the fix used caused bug 208688. 139209 needs to be fixed by counting lines instead.
CCBUG:208688
CCBUG:139209

backport revision 1028512 by je4d from trunk
------------------------------------------------------------------------
r1028720 | kkofler | 2009-09-28 00:29:44 +0000 (Mon, 28 Sep 2009) | 5 lines

track line counts so that we don't interpret a diff header (--- file...) as a removal if there's nothing separating it from the last hunk of the previous diff.
CCBUG: 139209
(The correct fix for #139209, which hopefully doesn't cause regressions.)

backport revision 1028515 by je4d from trunk
------------------------------------------------------------------------
r1029839 | sengels | 2009-09-30 21:30:14 +0000 (Wed, 30 Sep 2009) | 1 line

undef ERROR on windows
------------------------------------------------------------------------
