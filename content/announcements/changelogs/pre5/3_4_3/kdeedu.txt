2005-08-02 11:47 +0000 [r442345]  cniehaus

	* branches/KDE/3.4/kdeedu/kalzium/src/plotwidget.cpp,
	  branches/KDE/3.4/kdeedu/kalzium/src/main.cpp: * Increase the
	  version number as this is a fix * Making the "Connect Points"
	  work again. It seems that this never worked in KDE 3.4.x... If
	  you are a packager, please make sure you pick this fix!
	  CCMAIL:109980

2005-08-10 07:08 +0000 [r444438]  mueller

	* branches/KDE/3.4/kdeedu/kvoctrain/kvoctrain/langen2kvtml: don't
	  use hardcoded paths

2005-08-15 12:27 +0000 [r449410]  pino

	* branches/KDE/3.4/kdeedu/kalzium/src/elementdataviewer.cpp: Take
	  the space for the y axis label. CCBUG: 109979

2005-08-25 21:25 +0000 [r453333]  ingwa

	* branches/KDE/3.4/kdeedu/kiten/learn.cpp,
	  branches/KDE/3.4/kdeedu/ktouch/src/ktouchstatistics.cpp,
	  branches/KDE/3.4/kdeedu/kig/filters/exporter.cc: Fix bug 111452:
	  Code violates C++ spec with improper const and iterator
	  declarations I will forward port this to 3.5 and trunk. BUGS:
	  111452

2005-08-31 19:00 +0000 [r455531]  mueller

	* branches/KDE/3.4/kdeedu/kig/misc/object_hierarchy.h: compile

2005-09-18 08:45 +0000 [r461597]  cniehaus

	* branches/KDE/3.4/kdeedu/kalzium/src/kalziumrc: * backported the
	  fix to kde 3.4 as well

2005-09-29 20:16 +0000 [r465448]  fedemar

	* branches/KDE/3.4/kdeedu/kmplot/kmplot/View.cpp: Backport my fix
	  for bug #113387.

2005-10-01 02:23 +0000 [r465866]  harris

	* branches/KDE/3.4/kdeedu/kstars/kstars/main.cpp,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/observinglist.cpp,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/observinglist.h:
	  Fixing BR #113611 (SIGSEGV in Observing List tool). Added a
	  "SkyObject *oCurrent" member to keep track of the
	  currently-selected object. THis had been fixed in the 3.5 branch
	  already, but I didn't backport because I didn't realize there
	  would be a 3.4.3 release. Also updated version number in the 3.4
	  branch to to 1.1.3 for the kde-3.4.3 release. BUG: 113611

2005-10-01 02:57 +0000 [r465868]  harris

	* branches/KDE/3.4/kdeedu/kstars/kstars/tools/observinglist.cpp:
	  One more fix in ObservingList: must check to make sure that
	  oCurrent is not NULL in slotClose().

2005-10-02 16:14 +0000 [r466415]  coolo

	* branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcdaylengthdlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/argsetlocaltime.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcplanetsdlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcapcoorddlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcequinoxdlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/planetviewerui.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcazeldlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/altvstimeui.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcjddlg.ui,
	  branches/KDE/3.4/kdeedu/kstars/kstars/tools/modcalcsidtimedlg.ui:
	  build with qt 3.3.5

2005-10-02 20:17 +0000 [r466495]  hedlund

	* branches/KDE/3.4/kdeedu/kwordquiz/src/kwordquizview.cpp: Backport
	  of fix for bug 93803. CCMAIL:lorenzheublein@web.de

2005-10-04 09:28 +0000 [r467086]  mueller

	* branches/KDE/3.4/kdeedu/kmplot/kmplot/diagr.cpp: fix undefined
	  operation

2005-10-05 13:47 +0000 [r467516]  coolo

	* branches/KDE/3.4/kdeedu/kdeedu.lsm: 3.4.3

