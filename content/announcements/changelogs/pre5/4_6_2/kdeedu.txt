------------------------------------------------------------------------
r1223184 | woebbe | 2011-03-01 12:19:42 +1300 (Tue, 01 Mar 2011) | 2 lines

link kdeui (and implicitly kdecore)

------------------------------------------------------------------------
r1223876 | scripty | 2011-03-06 03:18:54 +1300 (Sun, 06 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1224254 | nienhueser | 2011-03-10 08:01:34 +1300 (Thu, 10 Mar 2011) | 2 lines

Increase (library) version number to 1.0.2 (0.11.2) for the next release.

------------------------------------------------------------------------
r1224587 | scripty | 2011-03-13 02:36:13 +1300 (Sun, 13 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1225423 | conet | 2011-03-21 10:26:20 +1300 (Mon, 21 Mar 2011) | 4 lines

REVIEW: 6622

Fix sunlight and timezone display in the worldclock plasmoid.
Backported to the stable branch.
------------------------------------------------------------------------
r1225917 | lueck | 2011-03-25 09:09:13 +1300 (Fri, 25 Mar 2011) | 1 line

backport from trunk r1222389: add missing i18n call
------------------------------------------------------------------------
r1226184 | scripty | 2011-03-28 03:05:22 +1300 (Mon, 28 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226562 | scripty | 2011-03-31 02:44:00 +1300 (Thu, 31 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
