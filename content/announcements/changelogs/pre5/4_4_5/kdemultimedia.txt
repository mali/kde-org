------------------------------------------------------------------------
r1133565 | scripty | 2010-06-02 13:59:57 +1200 (Wed, 02 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1136893 | mpyne | 2010-06-11 14:35:26 +1200 (Fri, 11 Jun 2010) | 4 lines

Backport revision 1124771 to KDE SC 4.4.5 (JuK doesn't show tooltip over filenames anymore).

This was fixed in trunk back in May but I forgot to backport it looks like.

------------------------------------------------------------------------
r1136894 | mpyne | 2010-06-11 14:40:14 +1200 (Fri, 11 Jun 2010) | 9 lines

Backport bugfix for bug 168998 (JuK history doesn't work correctly) to KDE SC 4.4.5.

This includes two fixes in trunk, an initial fix making the history playlist get tracks
added again, and another fix that re-enables the custom column the history playlist was
supposed to have.

CCBUG:168998
FIXED-IN:4.4.5

------------------------------------------------------------------------
r1136896 | mpyne | 2010-06-11 14:41:54 +1200 (Fri, 11 Jun 2010) | 2 lines

SVN_SILENT Bump juk version for KDE SC 4.4.5.

------------------------------------------------------------------------
r1142136 | lueck | 2010-06-24 19:14:39 +1200 (Thu, 24 Jun 2010) | 1 line

screenshot update, backport from trunk
------------------------------------------------------------------------
