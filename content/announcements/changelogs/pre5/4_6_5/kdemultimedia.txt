------------------------------------------------------------------------
r1234940 | kylafas | 2011-06-04 01:23:22 +1200 (Sat, 04 Jun 2011) | 17 lines

juk: show only the selected columns in new playlists.

Whenever a new playlist was created (e.g. "File->New->Empty Playlist", a
DynamicPlaylist is created etc.), all the available columns were shown
instead of only those that the user had selected.

The problem was that slotInitialize() (which adds all the columns to the
playlist) was called _after_ Playlist::SharedSettings::apply() (which
sets the visible and hidden columns).

So, make sure slotInitialize() is called during the Playlist ctor.

This is a backport of commit 1234560 to branch 4.6, which will be
released as 4.6.5. So, bump JuK's version as well.

BUG: 243449
FIXED-IN: 4.6.5
------------------------------------------------------------------------
r1235483 | kylafas | 2011-06-06 14:00:14 +1200 (Mon, 06 Jun 2011) | 18 lines

juk: load search playlists even when containing empty patterns.

When a new search playlist is created, we are allowed to not fill any
"search lines" with our criteria. This means that when a new search
playlist is created/saved, it may contain search components with empty
patterns. However, when loading a search playlist from disk, we signal
an error if we read an empty search pattern. As a result, the playlist
does not get loaded and JuK gives the impression that it does not save
the search playlists.

Since we are presented with two search lines when creating a new search
playlist (see AdvancedSearchDialog's ctor), the above has a rather high
chance of happening.

So, upon loading, we should not consider the presense of empty patterns
as an error.

This is a backport of r1235482 from trunk to branch 4.6 (KDE SC 4.6.5).
------------------------------------------------------------------------
r1235753 | kylafas | 2011-06-09 12:48:30 +1200 (Thu, 09 Jun 2011) | 13 lines

juk: keep the order of tracks when loading a plain playlist.

Plain Playlists are saved by explicitly writting the filenames of the
tracks they contain (in contrast, SearchPlaylists store only their
search pattern and FolderPlaylists only their folder). The filenames are
written to the disk in the order they appear in the playlist.

So, when loading such Playlists, disable sorting before adding the
tracks, so that they appear in the order they were saved.

This is a backport of r1235751 from trunk to branch 4.6 (KDE SC 4.6.5).

CCBUG: 70402
------------------------------------------------------------------------
r1236078 | kylafas | 2011-06-11 17:34:10 +1200 (Sat, 11 Jun 2011) | 14 lines

juk: save the collection list cache no matter how the app exits.

Cache::save() was called from JuK::slotQuit(), which was triggered only
by "File->Quit". So, the cache was not saved when closing the window or
right-clicking the system tray and selecting "Quit",

JuK::queryExit() is called in every case, so put Cache::save() there.
Besides, according to KMainWindow's documentation, "a typical usage of
queryExit() is to write configuration data back".

While at it, remove saveConfig() from slotQuit(), since it is already
being called from queryExit().

This is a backport of r1236077 from trunk to branch 4.6 (KDE SC 4.6.5).
------------------------------------------------------------------------
r1238280 | scripty | 2011-06-25 15:54:48 +1200 (Sat, 25 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1238534 | kylafas | 2011-06-28 09:24:00 +1200 (Tue, 28 Jun 2011) | 14 lines

juk: fix crash when adding items to toolbars with RMB.

When adding an item to a toolbar using right-click, the KXMLGUIClient
object of the global action collection is accessed.
(see KMenuMenuHandler::slotAddToToolBar() )
The way the action collection was created meant the object was NULL,
which resulted in crashes.
So, create the action collection with KXMLGUIClient::actionCollection(),
which properly sets the object.

This is a backport of r1237516 from trunk to branch 4.6 (KDE SC 4.6.5).

BUG: 258641
FIXED-IN: 4.6.5
------------------------------------------------------------------------
r1238537 | kylafas | 2011-06-28 10:00:59 +1200 (Tue, 28 Jun 2011) | 17 lines

juk: no crash-on-exit while playing, no matter how we exit.

Since r921339, we stop the playback during exit, otherwise some phonon
backends will crash. However, the relevant code was only executed when
selecting "File->Quit", not when closing the window nor when quitting
from the system tray.

So, similarly to r1236077, put the stopping of playback into queryExit()
to get executed in every case.

Being in queryExit() means quit() has already been called. So, we can no
longer wait for Phonon's StoppedState and then call quit(), as was done
in r921339.
However, waiting for Phonon's state change does not seem to be necessary
any more, so we can safely remove slotPlaybackStopped().

This is a backport of r1236082 from trunk to branch 4.6 (KDE SC 4.6.5).
------------------------------------------------------------------------
r1239249 | scripty | 2011-07-04 16:13:31 +1200 (Mon, 04 Jul 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1239952 | woebbe | 2011-07-06 18:02:15 +1200 (Wed, 06 Jul 2011) | 4 lines

Backport of rev. 1238217:

ffmpegthumbs: Fixed Bug 276375 and let ffmpegthumbs compile against ffmpeg 0.8

------------------------------------------------------------------------
