Dir: kdeaddons
----------------------------
new version numbers



Dir: kdeaddons/debian
----------------------------
Oops.
----------------------------
A round of fixes for 3.3.1.
----------------------------
Remove the rellinks plugin until it's unbroken.
----------------------------
Final changes for the kdeaddons upload.
----------------------------
Fix source overrides (see #261435).
----------------------------
Install docs for individual plugins/etc.
----------------------------
Manpage updates.
----------------------------
Finished 3.3 license audit.



Dir: kdeaddons/kfile-plugins/lnk
----------------------------
Instead of putting all drive and path information together into
info.driveName and leaving info.path empty, split it appropriately between
info.driveName and info.path.



Dir: kdeaddons/kicker-applets/math
----------------------------
fix compile (gcc 4.0)



Dir: kdeaddons/konq-plugins/arkplugin
----------------------------
fix compile (gcc 4.0)



Dir: kdeaddons/konq-plugins/kuick
----------------------------
Escape '&'
BUG: 94002



Dir: kdeaddons/konq-plugins/rellinks
----------------------------
Backport of CPU-hog fix
----------------------------
Intelligent destructor? Oh no.... Stop-gap fix: don't crash if the toolbar was deleted before us.
BUG: 93599



Dir: kdeaddons/konq-plugins/sidebar/mediaplayer
----------------------------
Also remove universal sidebar default
