2006-01-20 06:54 +0000 [r500403]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/examples/change_interval/interval.py
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/change_interval
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/setIncomingData
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/service_group/service_group.theme
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/setIncomingData/1.theme
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/mouseDrop/mousedrop.theme
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/setIncomingData/2.theme
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/service_group
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/mouseDrop
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/change_interval/interval.theme
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/service_group/service_group.py
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/setIncomingData/1.py
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/mouseDrop/mousedrop.py
	  (added),
	  branches/KDE/3.5/kdeutils/superkaramba/examples/setIncomingData/2.py
	  (added): More examples showing how to use the new functions

2006-01-20 11:57 +0000 [r500489]  pdamsten

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/systray_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/widget_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/svcgrp_python.cpp: Fix
	  compile warnings.

2006-01-28 12:30 +0000 [r503134]  deller

	* branches/KDE/3.5/kdeutils/superkaramba/src/themesdlg.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/superkaramba.kcfg: use
	  writePathEntry() and friends

2006-01-30 20:57 +0000 [r503951]  mueller

	* branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kvaio.cpp: avoid
	  empty if() body warning

2006-01-30 21:17 +0000 [r503958]  kossebau

	* branches/KDE/3.5/kdeutils/khexedit/parts/kpart/khexedit2partui.rc:
	  BUG:121036 default names from ui_standards.rc are not taken if
	  not already in use by the embedding shell

2006-02-03 19:22 +0000 [r505389]  jriddell

	* branches/KDE/3.5/kdeutils/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeutils
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeutils

2006-02-08 16:33 +0000 [r507141]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.h,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.h: This
	  fixes hopefully numerous bugs related to precision in Hex-mode.

2006-02-08 16:59 +0000 [r507155]  hindenburg

	* branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: Use QMutex
	  instead of file locking. BUG:121048

2006-02-09 17:24 +0000 [r507617]  hindenburg

	* branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: Revert last
	  change...doesn't allow multiple instances.... CCBUG: 121048

2006-02-13 17:13 +0000 [r509076]  pdamsten

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp: Trying to
	  fix 'moving themes' bug.

2006-02-13 17:33 +0000 [r509081]  pdamsten

	* branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.cpp:
	  Specify file permissions when creating lock file.

2006-02-13 18:29 +0000 [r509117]  annma

	* branches/KDE/3.5/kdeutils/doc/kcalc/index.docbook: BUG=120152

2006-02-13 20:57 +0000 [r509166]  annma

	* branches/KDE/3.5/kdeutils/doc/kcalc/commands.docbook (added),
	  branches/KDE/3.5/kdeutils/doc/kcalc/index.docbook: add menus
	  description chapter if someone could review that would be great,
	  thanks BUG=72981

2006-02-13 22:07 +0000 [r509194]  annma

	* branches/KDE/3.5/kdeutils/doc/kcalc/index.docbook: remove
	  obsolete doc BUG=120812

2006-02-15 23:52 +0000 [r509985]  annma

	* branches/KDE/3.5/kdeutils/doc/KRegExpEditor/index.docbook,
	  branches/KDE/3.5/kdeutils/doc/kwallet/index.docbook: fill in
	  empty <para> tags for KWallet doc remove an empty <para> for
	  KRegExpEditor doc

2006-02-18 14:53 +0000 [r510943-510942]  henrique

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp: * Use
	  gzip/gunzip if compress/uncompress are not available. Based on
	  patch by Henry Kay <haris@mpa.gr> BUG: 121601

	* branches/KDE/3.5/kdeutils/ark/main.cpp: * Increase the version
	  number

2006-02-22 15:15 +0000 [r512452]  pfeiffer

	* branches/KDE/3.5/kdeutils/kedit/kedit.cpp: kdeui's KEdit sets
	  auto-hiding already, no need to do it here as well

2006-02-24 11:56 +0000 [r513065]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/ar.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.h,
	  branches/KDE/3.5/kdeutils/ark/zip.cpp,
	  branches/KDE/3.5/kdeutils/ark/sevenzip.cpp,
	  branches/KDE/3.5/kdeutils/ark/compressedfile.cpp,
	  branches/KDE/3.5/kdeutils/ark/lha.cpp,
	  branches/KDE/3.5/kdeutils/ark/rar.cpp,
	  branches/KDE/3.5/kdeutils/ark/tar.cpp,
	  branches/KDE/3.5/kdeutils/ark/zoo.cpp,
	  branches/KDE/3.5/kdeutils/ark/ace.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Distinguish compression
	  and decompression utilities by introducing two new functions:
	  verifyCompressUtilityIsAvailable and verifyDecompressUtilityI
	  sAvailable. This way for example we can use "zip" to compress a
	  file even when " unzip" is not installed BUG: 86854

2006-02-24 22:54 +0000 [r513315]  charis

	* branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.cpp: We should use
	  KGpgSettings::writeConfig() in order for the changes to be
	  actually written in the config file BUG: 99395

2006-02-25 00:51 +0000 [r513327]  henrique

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp,
	  branches/KDE/3.5/kdeutils/ark/filelistview.h: * Change the view
	  item sorting so that folders are shown first. CCBUG: 122663

2006-02-25 03:14 +0000 [r513340]  charis

	* branches/KDE/3.5/kdeutils/ark/arch.cpp: * If user presses cancel
	  in view before password, don't try to open the file * If user
	  fails the password and then tries to view the file, don't say
	  incorrectly that he has failed the pass CCBUG: 119056

2006-02-25 19:44 +0000 [r513612]  charis

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp: * When a
	  directory to be deleted has children add both its children and
	  the directory * Check if an item's parent actually has siblings
	  to avoid crash BUG: 116374

2006-03-03 18:29 +0000 [r515433]  woebbe

	* branches/KDE/3.5/kdeutils/ark/arch.h: -pedantic

2006-03-04 04:21 +0000 [r515542]  jlee

	* branches/KDE/3.5/kdeutils/kjots/kjotsentry.cpp: BUG:122876
	  Encoding is not being stored correctly in the XML. For now, use a
	  QTextStream to read it in properly.

2006-03-04 14:59 +0000 [r515667]  jlee

	* branches/KDE/3.5/kdeutils/kjots/KJotsMain.cpp: Prevent data
	  corruption in the event that a user changes the encoding setting,
	  but doesn't dirty the book afterwards so it is not rewritten with
	  the new encoding.

2006-03-04 15:31 +0000 [r515674]  jlee

	* branches/KDE/3.5/kdeutils/kjots/KJotsMain.cpp:
	  CCMAIL:aseigo@kde.org Add keyboard shortcuts for deleting
	  pages/books.

2006-03-04 15:36 +0000 [r515676]  henrique

	* branches/KDE/3.5/kdeutils/ark/tar.cpp,
	  branches/KDE/3.5/kdeutils/ark/tarlistingthread.cpp: * Fix opening
	  of tarballs CCMAIL: mmodem00@netvisao.pt

2006-03-04 16:22 +0000 [r515688]  henrique

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp: * According to
	  Nicolas Goutte, gzip cannot create compress-compatible files.
	  CCBUG: 121601

2006-03-04 16:52 +0000 [r515691]  henrique

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp: * Fix a crash
	  while deleting items. Haris, your fix for 116374 was not working
	  correctly, you needed to test for item->parent()->parent()
	  instead of item->parent()->nextSibling(). BUG: 121937 CCBUG:
	  116374 CCMAIL: haris@mpa.gr

2006-03-04 20:12 +0000 [r515772]  henrique

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/arkwidget.h,
	  branches/KDE/3.5/kdeutils/ark/extractiondialog.cpp: * Don't open
	  the extraction folder when the extraction operation fails. BUG:
	  123091

2006-03-04 21:07 +0000 [r515787]  henrique

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp: * Improve column
	  sizing. BUG: 119423

2006-03-04 21:19 +0000 [r515792]  henrique

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.h: * Disable the view while
	  busy. * Make the CRC left-aligned.

2006-03-05 13:19 +0000 [r515942]  henrique

	* branches/KDE/3.5/kdeutils/ark/rar.cpp: * Fix opening of RAR
	  archives

2006-03-05 13:23 +0000 [r515945]  henrique

	* branches/KDE/3.5/kdeutils/ark/sevenzip.cpp: * Fix opening of
	  7-zip archives

2006-03-05 17:25 +0000 [r516007]  henrique

	* branches/KDE/3.5/kdeutils/ark/ark_part.cpp,
	  branches/KDE/3.5/kdeutils/ark/rar.cpp,
	  branches/KDE/3.5/kdeutils/ark/ark_part.h,
	  branches/KDE/3.5/kdeutils/ark/arkutils.cpp: * Give names to some
	  widgets * ArkUtils::fixYear should do nothing if the year already
	  has four digits * Fix opening of RAR archives when only unrar is
	  installed CCMAIL: mseiwert@hbv.de

2006-03-05 22:58 +0000 [r516104]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: Strip "../" to match
	  the filename in /tmp BUG: 85084

2006-03-10 21:02 +0000 [r517381]  henrique

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp,
	  branches/KDE/3.5/kdeutils/ark/filelistview.h: * Proper fix for
	  bug #122663 (Show folders first) CCBUG:119423 CCBUG:122663

2006-03-11 12:42 +0000 [r517553]  charis

	* branches/KDE/3.5/kdeutils/ark/rar.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Patch by Simon Raffeiner,
	  fixes the extraction on password protected RARs BUG: 115152
	  CCMAIL: sturmflut@lieberbiber.de

2006-03-12 01:00 +0000 [r517741]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: When an archive
	  contains just one file, show the generic extraction dialog and
	  not the "extract selected or all files" one

2006-03-12 05:32 +0000 [r517756]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.h: BUG:121468
	  Revert this fix since it causes unexpected side effects such as:
	  - hide/show displays an item in the taskbar like a normal window
	  - themes can't be under a dock bar such as kicker - themes can't
	  be moved off screen - if kicker is not full length, themes can't
	  be in the visually empty areas as if the kicker was full length
	  still These reasons are enough for me to search for an
	  alternative workaround, but a true implementation may not be
	  possible until there is a specific window type for desktop
	  applets in the future, i.e. Plasma/KDE4.

2006-03-12 18:22 +0000 [r517974]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: Better coding style

2006-03-13 23:52 +0000 [r518412]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp: This
	  handles the lowering of themes when opened from the theme dialog
	  much better than relying on lower() since KWin::lowerWindow(
	  winId() ) is more direct. This resolves the problem when if
	  multiple themes are opened with the theme dialog, they remain
	  above other windows when they should go directly to the
	  background.

2006-03-14 00:19 +0000 [r518417]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/AUTHORS: Update to include
	  people that have been helping and supplying patches over the
	  years.

2006-03-15 14:55 +0000 [r518871]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: Bump up the
	  version to 0.39 before the 3.5.2 tagging occurs.

2006-03-16 01:01 +0000 [r519039]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/arkviewer.h,
	  branches/KDE/3.5/kdeutils/ark/arkwidget.h,
	  branches/KDE/3.5/kdeutils/ark/arkviewer.cpp: Don't use QString's
	  for filenames, use KURL instead to escape bad characters BUG:
	  114819

2006-03-17 00:42 +0000 [r519386]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/arkwidget.h: Make the pointer to
	  the list global, delete it only when the whole procedure is done
	  BUG: 119056

2006-03-17 01:22 +0000 [r519393]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: Small fix for my
	  previous commit CCBUG: 119056

2006-03-17 21:34 +0000 [r519791]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: tagging 3.5.2

