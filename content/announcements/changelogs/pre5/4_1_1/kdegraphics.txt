------------------------------------------------------------------------
r837136 | gateau | 2008-07-23 23:22:38 +0200 (Wed, 23 Jul 2008) | 4 lines

Fix cropping of images large enough to be down sampled.
CCMAIL: me@mornfall.net
Backported https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@837135

------------------------------------------------------------------------
r837342 | pino | 2008-07-24 15:32:25 +0200 (Thu, 24 Jul 2008) | 4 lines

backport: properly keep a reference to the print option widget, and create it only when necessary

CCBUG: 167353

------------------------------------------------------------------------
r837638 | chehrlic | 2008-07-25 12:09:13 +0200 (Fri, 25 Jul 2008) | 1 line

win32/mingw compile++
------------------------------------------------------------------------
r837831 | aacid | 2008-07-25 22:25:23 +0200 (Fri, 25 Jul 2008) | 2 lines

Backport r837829 okular/trunk/KDE/kdegraphics/okular/generators/poppler/generator_pdf.cpp: delete pdfConv

------------------------------------------------------------------------
r837843 | pino | 2008-07-25 22:52:21 +0200 (Fri, 25 Jul 2008) | 2 lines

backport: hopefully make it compile and work also where qreal is not double but float

------------------------------------------------------------------------
r838788 | nlecureuil | 2008-07-28 20:13:58 +0200 (Mon, 28 Jul 2008) | 1 line

Backport KExiv2::supportTiffWritting ( needed to compile digikam against kde 4.1)
------------------------------------------------------------------------
r839381 | aacid | 2008-07-30 00:05:38 +0200 (Wed, 30 Jul 2008) | 6 lines

Backport r839378

Repeat with me, it's image/jp2, not image/jpeg2000

And now i can see jp2 images in okular and gwenview :-)

------------------------------------------------------------------------
r839567 | pino | 2008-07-30 12:57:59 +0200 (Wed, 30 Jul 2008) | 2 lines

backport: when saving the internal xml, be sure to save it as utf-8, and to advertize the encoding in the main processing instruction

------------------------------------------------------------------------
r839617 | pino | 2008-07-30 14:24:27 +0200 (Wed, 30 Jul 2008) | 4 lines

backport: do not assume there is always a configuration dialog around (eg, when doing a print preview)

CCBUG: 167734

------------------------------------------------------------------------
r839973 | pino | 2008-07-31 11:27:13 +0200 (Thu, 31 Jul 2008) | 2 lines

backport: more qreal/double mismatches

------------------------------------------------------------------------
r841348 | pino | 2008-08-03 10:44:08 +0200 (Sun, 03 Aug 2008) | 2 lines

backport: update all the configuration (not just the AA settings) each time a document is open

------------------------------------------------------------------------
r841925 | pino | 2008-08-04 12:21:00 +0200 (Mon, 04 Aug 2008) | 5 lines

Backport: enable the text-to-speech actions only when there is a document loaded (other than KTTS being present, of course).
This will be in KDE 4.1.1.

CCBUG: 168310

------------------------------------------------------------------------
r842058 | osterfeld | 2008-08-04 18:34:08 +0200 (Mon, 04 Aug 2008) | 1 line

find exiv2 includes in non-standard path
------------------------------------------------------------------------
r842100 | osterfeld | 2008-08-04 19:49:35 +0200 (Mon, 04 Aug 2008) | 1 line

add NOGUI, preventing app bundle on OS X
------------------------------------------------------------------------
r842325 | pino | 2008-08-05 11:16:55 +0200 (Tue, 05 Aug 2008) | 5 lines

Backport: correctly get the filename from the original url, not from the local file name of the real file being open.
Will be in KDE 4.1.1.

CCBUG: 168068

------------------------------------------------------------------------
r842611 | sars | 2008-08-05 18:22:01 +0200 (Tue, 05 Aug 2008) | 3 lines

Fix to be more sane standard compliant. Some backends, like pixma, require sane_cancel() after a scan, before parameters can be changed.

CCBUG:167246
------------------------------------------------------------------------
r842619 | sars | 2008-08-05 18:42:34 +0200 (Tue, 05 Aug 2008) | 3 lines

Fix crash when trying to select a preview area when the backend does not support scan area selection.

CCBUG:167248
------------------------------------------------------------------------
r842847 | pino | 2008-08-06 06:18:44 +0200 (Wed, 06 Aug 2008) | 5 lines

Backport workaround Leon Bottou's workaround for DjVuLibre crash.
Will be in KDE 4.1.1.

CCBUG: 168030

------------------------------------------------------------------------
r844887 | aacid | 2008-08-10 22:51:36 +0200 (Sun, 10 Aug 2008) | 5 lines

Backport  r844880 okular/trunk/KDE/kdegraphics/okular/ui/pageview.cpp:
If we are cancelling move, make sure we are where we should be

BUG: 165809

------------------------------------------------------------------------
r845353 | gateau | 2008-08-11 16:30:30 +0200 (Mon, 11 Aug 2008) | 5 lines

Make fileItemForIndex and urlForIndex more robust.
Use them in thumbnailForIndex.
BUG:168838
Backported https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@845348

------------------------------------------------------------------------
r845357 | gateau | 2008-08-11 16:34:10 +0200 (Mon, 11 Aug 2008) | 2 lines

Bumped version number.

------------------------------------------------------------------------
r847087 | abizjak | 2008-08-14 20:00:27 +0200 (Thu, 14 Aug 2008) | 2 lines

Don't draw resize lines over parts of the user interface

------------------------------------------------------------------------
r847713 | scripty | 2008-08-16 08:06:26 +0200 (Sat, 16 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r848554 | scripty | 2008-08-18 07:08:07 +0200 (Mon, 18 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r849951 | pino | 2008-08-20 16:09:26 +0200 (Wed, 20 Aug 2008) | 5 lines

Backport: be sure to close the temporary PS file before sending it to lpr, so it is all flushed.
Will be in KDE 4.1.1.

CCBUG: 160860

------------------------------------------------------------------------
r850146 | pino | 2008-08-20 22:56:07 +0200 (Wed, 20 Aug 2008) | 4 lines

Backport: take into account relative links in pages, and resolve them correctly.

CCBUG: 169001

------------------------------------------------------------------------
r851326 | pino | 2008-08-23 16:40:25 +0200 (Sat, 23 Aug 2008) | 2 lines

bump versions

------------------------------------------------------------------------
r851471 | uwolfer | 2008-08-23 20:20:04 +0200 (Sat, 23 Aug 2008) | 4 lines

Backport:
SVN commit 851470 by uwolfer:

add missing separator
------------------------------------------------------------------------
r852003 | scripty | 2008-08-25 07:25:51 +0200 (Mon, 25 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r852127 | rich | 2008-08-25 13:50:17 +0200 (Mon, 25 Aug 2008) | 4 lines

- Backport the config saving fix, didn't I do this once already?

CCMAIL: 164729-done@bugs.kde.org

------------------------------------------------------------------------
r852756 | lueck | 2008-08-26 16:28:44 +0200 (Tue, 26 Aug 2008) | 2 lines

backport from trunk 852742 (fixed wrong X11 test variable to install the docs, when the kgamma is installed)
and 852741 (fixed wrong X-DocPath)
------------------------------------------------------------------------
r853065 | scripty | 2008-08-27 08:04:57 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853157 | ilic | 2008-08-27 13:30:04 +0200 (Wed, 27 Aug 2008) | 1 line

Extract PO to match component name. (bport: 853156)
------------------------------------------------------------------------
r853349 | sengels | 2008-08-27 17:42:37 +0200 (Wed, 27 Aug 2008) | 1 line

fix searching for libspectre on windows
------------------------------------------------------------------------
r853404 | sengels | 2008-08-27 19:14:24 +0200 (Wed, 27 Aug 2008) | 1 line

make these two ready for msvc
------------------------------------------------------------------------
r853444 | ereslibre | 2008-08-27 20:34:06 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport of rev 853433. Call to setupGUI after createGUI has been called

------------------------------------------------------------------------
r853469 | aacid | 2008-08-27 21:07:47 +0200 (Wed, 27 Aug 2008) | 4 lines

Backport r853468 | aacid | 2008-08-27 21:06:02 +0200 (Wed, 27 Aug 2008) | 2 lines

fix wrong usage of QByteArray::fromRawData

------------------------------------------------------------------------
