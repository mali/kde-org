------------------------------------------------------------------------
r1108172 | scripty | 2010-03-28 15:38:22 +1300 (Sun, 28 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1109774 | scripty | 2010-04-01 15:11:01 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110431 | scripty | 2010-04-03 14:52:02 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111188 | scripty | 2010-04-05 13:49:26 +1200 (Mon, 05 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1112110 | annma | 2010-04-07 23:47:46 +1200 (Wed, 07 Apr 2010) | 4 lines

fix Picture of the Day display, thanks to Etienne!
BUG=226230


------------------------------------------------------------------------
r1112165 | annma | 2010-04-08 02:08:37 +1200 (Thu, 08 Apr 2010) | 2 lines

changes like border or round corners can also happen in PoTD mode

------------------------------------------------------------------------
r1112579 | annma | 2010-04-09 03:14:33 +1200 (Fri, 09 Apr 2010) | 2 lines

make it work again 

------------------------------------------------------------------------
r1112635 | annma | 2010-04-09 06:02:50 +1200 (Fri, 09 Apr 2010) | 2 lines

fix provider

------------------------------------------------------------------------
r1113599 | annma | 2010-04-11 19:48:29 +1200 (Sun, 11 Apr 2010) | 2 lines

fix loading of picture and comment debug output

------------------------------------------------------------------------
r1113867 | scripty | 2010-04-12 13:41:04 +1200 (Mon, 12 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115492 | mfuchs | 2010-04-17 03:50:25 +1200 (Sat, 17 Apr 2010) | 2 lines

Backport r1115491
Adds kError and kWarning to know which errors happened.
------------------------------------------------------------------------
r1115518 | mfuchs | 2010-04-17 05:24:14 +1200 (Sat, 17 Apr 2010) | 2 lines

Backport r1115517
Uses an kError if no identifier has been specified or if the engine could not be created correctly.
------------------------------------------------------------------------
r1116275 | scripty | 2010-04-19 13:53:02 +1200 (Mon, 19 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117338 | scripty | 2010-04-22 09:38:56 +1200 (Thu, 22 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117569 | annma | 2010-04-23 02:09:48 +1200 (Fri, 23 Apr 2010) | 2 lines

make the first PoTD appear in the combobox right away

------------------------------------------------------------------------
r1117580 | annma | 2010-04-23 02:29:43 +1200 (Fri, 23 Apr 2010) | 2 lines

revert previous commit which was pretty stupid and set a default PoTD so to get the combo show it

------------------------------------------------------------------------
r1117791 | scripty | 2010-04-23 13:58:29 +1200 (Fri, 23 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118339 | mart | 2010-04-25 01:04:09 +1200 (Sun, 25 Apr 2010) | 2 lines

backport fixing of the flickr provider

------------------------------------------------------------------------
r1118541 | scripty | 2010-04-25 13:58:55 +1200 (Sun, 25 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119180 | zander | 2010-04-27 06:35:58 +1200 (Tue, 27 Apr 2010) | 2 lines

Make compile with Qt47

------------------------------------------------------------------------
