------------------------------------------------------------------------
r1107867 | scripty | 2010-03-27 15:34:55 +1300 (Sat, 27 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108171 | scripty | 2010-03-28 15:38:13 +1300 (Sun, 28 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108841 | laidig | 2010-03-30 10:51:27 +1300 (Tue, 30 Mar 2010) | 6 lines

backport r1108839: fix the sorting by correctly resetting the sort flag

patch by Benjamin Schleinzer
thanks!

CCBUG:232556
------------------------------------------------------------------------
r1109773 | scripty | 2010-04-01 15:10:54 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110083 | rdieter | 2010-04-02 09:24:01 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1097646 , fix for Qt 4.7

------------------------------------------------------------------------
r1110356 | gladhorn | 2010-04-03 09:47:48 +1300 (Sat, 03 Apr 2010) | 4 lines

backport relevant parts of 1110354 without string changes for KDE 4.4.3
Always set the grade to 1 when an answer was wrong.
CCBUG: 161048

------------------------------------------------------------------------
r1110362 | gladhorn | 2010-04-03 10:04:08 +1300 (Sat, 03 Apr 2010) | 3 lines

backport 1110361
save progress in practice when returning to welcome screen

------------------------------------------------------------------------
r1110364 | laidig | 2010-04-03 10:07:00 +1300 (Sat, 03 Apr 2010) | 3 lines

backport r1110363: apply the table font

CCBUG:232654
------------------------------------------------------------------------
r1110382 | gladhorn | 2010-04-03 10:54:42 +1300 (Sat, 03 Apr 2010) | 4 lines

fix duplicate upload in menu for 4.4.3
this was already fixed in trunk
BUG: 230749

------------------------------------------------------------------------
r1110430 | scripty | 2010-04-03 14:51:56 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1112828 | sitter | 2010-04-09 21:29:15 +1200 (Fri, 09 Apr 2010) | 3 lines

Backport r1112824.
Fixing cantor part showing up in menu by Alessandro Ghersi

------------------------------------------------------------------------
r1113381 | gladhorn | 2010-04-11 05:23:17 +1200 (Sun, 11 Apr 2010) | 3 lines

backport 1113380
setting of first sound was broken

------------------------------------------------------------------------
r1113643 | schleinzer | 2010-04-11 23:28:53 +1200 (Sun, 11 Apr 2010) | 3 lines

backport 1113642
fixed kvtml file writer bug for pairs

------------------------------------------------------------------------
r1118063 | nienhueser | 2010-04-24 07:52:32 +1200 (Sat, 24 Apr 2010) | 3 lines

Fix gpsd data interpretation for libgps >= 2.90 (wrong flag used to determine if data is available). Backport of commit 1117994.
CCBUG: 233603

------------------------------------------------------------------------
r1118103 | nienhueser | 2010-04-24 10:44:27 +1200 (Sat, 24 Apr 2010) | 3 lines

Add the WATCH_NMEA flag needed for version 2.93 and later to work. Backport of commit 1118101.
CCBUG: 233603

------------------------------------------------------------------------
r1118226 | scripty | 2010-04-24 14:04:22 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118404 | khudyakov | 2010-04-25 04:32:34 +1200 (Sun, 25 Apr 2010) | 4 lines

Backport bug fix into 4.4 branch 

BUG: 229994

------------------------------------------------------------------------
r1120292 | nienhueser | 2010-04-29 07:40:23 +1200 (Thu, 29 Apr 2010) | 3 lines

Use a non-blocking call to determine if data is available to avoid that the thread cannot be terminated properly when it's blocked by gpsd. Backport of commit 1120287.
CCBUG: 234311

------------------------------------------------------------------------
r1120312 | jmhoffmann | 2010-04-29 08:58:31 +1200 (Thu, 29 Apr 2010) | 2 lines

Fix mail address and type in name.

------------------------------------------------------------------------
r1120313 | jmhoffmann | 2010-04-29 08:58:55 +1200 (Thu, 29 Apr 2010) | 2 lines

Bump version for 4.4.3 release.

------------------------------------------------------------------------
