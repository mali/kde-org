---
aliases:
- /announcements/plasma-5.10.4-5.10.5-changelog
hidden: true
plasma: true
title: Plasma 5.10.5 Complete Changelog
type: fulllog
version: 5.10.5
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Auto generate desktop file for url handling. <a href='https://commits.kde.org/discover/b433b503016e5028e1c6daf752fe2a6c09b23b04'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7343'>D7343</a>
- Remove the navigationEnabled concept. <a href='https://commits.kde.org/discover/37cfd9b58e3bf1853e03f799e536e30ce755207c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383294'>#383294</a>
- Fix license, comply with our naming scheme. <a href='https://commits.kde.org/discover/217316656466f2b322116c23ac8da9e546b4d119'>Commit.</a>
- Don't disable the updates action when there are no updates. <a href='https://commits.kde.org/discover/e28b98bb945b1067f64632159c91c2b0113b9c8d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383294'>#383294</a>
- Some elements aren't reporting a big preview. <a href='https://commits.kde.org/discover/036bc1d10dc2c52d9226507a1f9c0c611dcb51dd'>Commit.</a>
- Fallback to the package icon if there isn't a stock icon as well. <a href='https://commits.kde.org/discover/98875579c1474acdf234dc10cd05df499aaf2f30'>Commit.</a>
- Prevent memory leak. <a href='https://commits.kde.org/discover/a77b6089724ffa8ebcb9d4083fc6f6bdda0691dc'>Commit.</a>
- Make sure we don't crash when opening flatpakrepo files. <a href='https://commits.kde.org/discover/01e9868e32a3ca71fde3ac8be13e5f49a62ee907'>Commit.</a>
- Make it possible to prevent some resources from being Reviewed. <a href='https://commits.kde.org/discover/a33e952d4f91a02a01e4211f5e02f38d6198c89e'>Commit.</a> See bug <a href='https://bugs.kde.org/383036'>#383036</a>
- Don't show the version if there's none on the ApplicationPage. <a href='https://commits.kde.org/discover/0a39fce84ac421d5875d7a182bc44394389a0457'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383036'>#383036</a>
- Make sure we don't render HTML on the Discover page header. <a href='https://commits.kde.org/discover/a32130def58d6acc26093abaf32e2b60c502047d'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Update comic KNS providers to https. <a href='https://commits.kde.org/kdeplasma-addons/351b46a1852f4a003b41fdaabf12f736d8111ed5'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a>

### <a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a>

- Make sure the dbus xml interface file exists before it's used. <a href='https://commits.kde.org/khotkeys/db3a04289d0f33285c3ca2d8fc05fc5bb45d608f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6792'>D6792</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Print plugin name in case of a loading error. <a href='https://commits.kde.org/kwin/bf2d7395880e626eee46017056bc352d90e2013f'>Commit.</a>
- Update KNS providers to https. <a href='https://commits.kde.org/kwin/308ab764286090326ffe8304acaf50afa7e74b16'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a>
- Call Platform::setupActionForGlobalAccel on the Client shortcut. <a href='https://commits.kde.org/kwin/4c996a57d4c01f092f9ed8f98a9f476c14c0c777'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6802'>D6802</a>
- [effects] Exclude OSD windows from desktop grid. <a href='https://commits.kde.org/kwin/cf62ac8039a5821e52c6fc8e6a7d45a38f350dd2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376987'>#376987</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6835'>D6835</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Set focus in this FocusScope. <a href='https://commits.kde.org/plasma-desktop/001c89d18ab737dd48451c73137004134fdc51b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383667'>#383667</a>
- Compress size hint changes before releasing position and repositioning item. <a href='https://commits.kde.org/plasma-desktop/3e3f0decc98304949c1f7cbb196bdf0686e75a08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382759'>#382759</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7385'>D7385</a>
- Update size hints less often. <a href='https://commits.kde.org/plasma-desktop/b8c1e0e3e060ff415f04b50cd5b7bcfbb8890dea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7384'>D7384</a>
- Disable bouncy scrolling. <a href='https://commits.kde.org/plasma-desktop/49fb504149e3ea3690616510ee313d33c7233d53'>Commit.</a> See bug <a href='https://bugs.kde.org/378042'>#378042</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7322'>D7322</a>
- [Folder View] Don't process mime data if immutable. <a href='https://commits.kde.org/plasma-desktop/d4bd4bfaf7013b92c1caf4c65ee4c6d3d6b65d25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383371'>#383371</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7235'>D7235</a>
- Fix group dialog no longer resizing/closing as windows are closed and the group is dissolved. <a href='https://commits.kde.org/plasma-desktop/a1e2ddd67a1226aa796c70fdfce3a8cf81a3f2e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382638'>#382638</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7035'>D7035</a>
- Prefer using https for kns providers. <a href='https://commits.kde.org/plasma-desktop/d6da5e89338afa23aba050fedfbb149b5a04bb26'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a>
- Kimpanel: Reduce the inputpanel flicker. <a href='https://commits.kde.org/plasma-desktop/64f9791cce2c8b5630a97cf37e3885328dec0575'>Commit.</a>
- Kimpanel: backport two commit not on 5.10 branch. <a href='https://commits.kde.org/plasma-desktop/4b9e4b651796a31cdaf1091f7c7a0cd1759c5197'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix QSortFilterProxyModelPrivate::updateChildrenMapping crash in libtaskmanager. <a href='https://commits.kde.org/plasma-workspace/d2f722a82ebeb213a89efc209ec726a8188de6f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381006'>#381006</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7139'>D7139</a>
- [Notifications] Clean up States. <a href='https://commits.kde.org/plasma-workspace/8f435a2dbde4c8f69800ed27878d89be0dfc8356'>Commit.</a> See bug <a href='https://bugs.kde.org/381105'>#381105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7319'>D7319</a>
- Don't change fillMode of image just before deleting it. <a href='https://commits.kde.org/plasma-workspace/a44d84ef47492ca60ee608996b5ab1f2849ef16e'>Commit.</a> See bug <a href='https://bugs.kde.org/381105'>#381105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7248'>D7248</a>
- Disable kuiserver debug output by default; make it appear in kdebugsettings. <a href='https://commits.kde.org/plasma-workspace/c67769e0963aef485b8e0d738ce67e63c532612c'>Commit.</a>
- [Battery Monitor] Fix icon size for InhibitionHint. <a href='https://commits.kde.org/plasma-workspace/03f9ab0f9149050541d2852f94b508f746aac955'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383213'>#383213</a>
- [Notifications] Improve mouse handling. <a href='https://commits.kde.org/plasma-workspace/7e2a29b0b18abe31df68c2f176124acfbc15c438'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382263'>#382263</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7029'>D7029</a>
- Use correct easing type values. <a href='https://commits.kde.org/plasma-workspace/1511e486ae46789a34506461f2847d4a37f605cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382834'>#382834</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6970'>D6970</a>
- Prefer using https for kns providers. <a href='https://commits.kde.org/plasma-workspace/ae943198bf74d563adcb1f3d36ee4ba1b7b274a9'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a>
- Improve unittest for renaming desktop icons. <a href='https://commits.kde.org/plasma-workspace/31bbfe5987698d39e806087e559e74b5fab70ed6'>Commit.</a> See bug <a href='https://bugs.kde.org/382341'>#382341</a>
- [AppMenu Applet] Pass "ctx" as context instead of "this". <a href='https://commits.kde.org/plasma-workspace/3ff8c1f65ebeb5c0b7c2d2af59c9330558fcdb5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382386'>#382386</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6951'>D6951</a>
- [Klipper ModelTest] Fix build. <a href='https://commits.kde.org/plasma-workspace/94f943b3ece7a5b14f9377ec5b3c43b55e9ac1c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6874'>D6874</a>
- [TasksModel] Use LauncherUrlWithoutIcon in move method. <a href='https://commits.kde.org/plasma-workspace/cc2f28ece16a8e83703ca0d1a074410f62d1c130'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6872'>D6872</a>
- [LauncherTasksModel] Emit dataChanged for LauncherUrlWithoutIcon on sycoca change. <a href='https://commits.kde.org/plasma-workspace/5bac69e51657dd3254d3aaf3cd6a543cd3079401'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6869'>D6869</a>

### <a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a>

- Fix implicit QString->QUrl conversion. <a href='https://commits.kde.org/plymouth-kcm/ee98d5bcc86188f69bda14ef295458c09ec10402'>Commit.</a>