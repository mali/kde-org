---
aliases:
- /announcements/plasma-5.19.1-5.19.2-changelog
hidden: true
plasma: true
title: Plasma 5.19.2 Complete Changelog
type: fulllog
version: 5.19.2
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

<ul id='uldiscover' style='display: block'>
<li>Confirm reboot action with the user. <a href='https://commits.kde.org/discover/250325b7e95cd0d81a04549c428ee6889265e111'>Commit.</a> </li>
<li>Odrs: improve the chances to find the crash. <a href='https://commits.kde.org/discover/3d873f60648355a5ba5cc2aaacbc46855b12d723'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419273'>#419273</a></li>
<li>Appstream: Try harder at de-duplicating v2. <a href='https://commits.kde.org/discover/985f7d5cfe5c2fbddaebe3b3cf60826bb99d500b'>Commit.</a> </li>
</ul>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

<ul id='ulkinfocenter' style='display: block'>
<li>Also localize the bit entry. <a href='https://commits.kde.org/kinfocenter/95ee875df46f0541c1e419bcf8bbccbfd3286df4'>Commit.</a> See bug <a href='https://bugs.kde.org/407179'>#407179</a></li>
<li>Make entry members protected. <a href='https://commits.kde.org/kinfocenter/5b3374f18f4fbefea88c2dc46f5fdb2fd522e8c0'>Commit.</a> </li>
<li>Fix English copy to clipboard version. <a href='https://commits.kde.org/kinfocenter/97b66813bcfaeec046fc36dd4f5a8021b2a55264'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407179'>#407179</a></li>
</ul>

### <a name='kwayland-server' href='https://commits.kde.org/kwayland-server'>KWayland-server</a>

<ul id='ulkwayland-server' style='display: block'>
<li>Scope dragAndDropActionsChanged to source lifespan. <a href='https://commits.kde.org/kwayland-server/5167c11b7dfe0520ef34ca0d237c480101eec17f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423127'>#423127</a></li>
</ul>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

<ul id='ulkwin' style='display: block'>
<li>Fix build with loadThemeCursor templates. <a href='https://commits.kde.org/kwin/34c1bccdb797d72c0f0db75ef87ec25695ac6d01'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423052'>#423052</a></li>
<li>KCM Compositing: Fix save state. <a href='https://commits.kde.org/kwin/a8979aa4e46cc60c63542c12a73e8c6f1fa74041'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423223'>#423223</a></li>
<li>KCMoptions: delay initialization after QObject creation in standalone. <a href='https://commits.kde.org/kwin/396a9da557c8825cc6de2ca9cfd26babf6a9a5b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423111'>#423111</a></li>
<li>[x11] Create egl scene textures with right size. <a href='https://commits.kde.org/kwin/96774e79e7931d1fe6db8bc2c615d61fef366ad3'>Commit.</a> </li>
<li>[x11] Provide physical dimensions for outputs. <a href='https://commits.kde.org/kwin/bc2df9f84be7ff12ab7e67ee7931eae1f5aeacce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422816'>#422816</a></li>
</ul>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

<ul id='ullibksysguard' style='display: block'>
<li>Remove leftPadding/rightPadding. <a href='https://commits.kde.org/libksysguard/ab0362b4cb5ce2cdffea09fac0f55ac47921db19'>Commit.</a> </li>
<li>Add missing ids. <a href='https://commits.kde.org/libksysguard/5795bb15464624da36eb2e4a102d5b485a95c2a6'>Commit.</a> </li>
<li>Make Sensor::shortName property actually return shortName. <a href='https://commits.kde.org/libksysguard/767d04b87ffd75d83095bf7d0187afae84d5fc85'>Commit.</a> </li>
<li>Fix searching in FacesConfig. <a href='https://commits.kde.org/libksysguard/6bccf33120ac85b2a74bebdb34f6cc6711d65438'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422835'>#422835</a></li>
<li>Expose better size hints. <a href='https://commits.kde.org/libksysguard/3a133b7067c36e0ec36ea400908c7425ecacfdac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422669'>#422669</a>. Fixes bug <a href='https://bugs.kde.org/422888'>#422888</a></li>
<li>Reset the page when reloaded. <a href='https://commits.kde.org/libksysguard/ca980e7297868956e668d65e91fec58029667282'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422871'>#422871</a></li>
</ul>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

<ul id='uloxygen' style='display: block'>
<li>[style] Fix crash during app tear down sequence. <a href='https://commits.kde.org/oxygen/c7c8a213214e30cac583ab3f82817c2f8185fbeb'>Commit.</a> </li>
</ul>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

<ul id='ulplasma-nm' style='display: block'>
<li>[applet] Remove styled text support from list items. <a href='https://commits.kde.org/plasma-nm/3c660d8e20bdea80b3613c02320d1688b677ad98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423020'>#423020</a></li>
</ul>

### <a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a>

<ul id='ulplasma-vault' style='display: block'>
<li>[applet] Restore former default action. <a href='https://commits.kde.org/plasma-vault/9eefb78c95bfe612336bdf8d7474340cea1960c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422917'>#422917</a></li>
</ul>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

<ul id='ulplasma-workspace' style='display: block'>
<li>[Notifications] Support description where only a value is set. <a href='https://commits.kde.org/plasma-workspace/09e73292aa3e1d07d531d01bcdd086ab6b7801e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423218'>#423218</a></li>
<li>Fix case of monitored service in startplasma's shutdown. <a href='https://commits.kde.org/plasma-workspace/aaed0138ca8feebb9d45b9c4a2dfd5df651ad972'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422870'>#422870</a></li>
<li>Fix faint outline for notification timeout indicator with newer charts. <a href='https://commits.kde.org/plasma-workspace/363d32fab30bd06025307139ac13d58bb8b0d611'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423079'>#423079</a></li>
<li>[applets/appmenu] Listen to more signals to ensure that appmenu applet can reliably catch the active window changing. <a href='https://commits.kde.org/plasma-workspace/fe302963030be0ef137d3b04343cdfbe75887669'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422786'>#422786</a>. Fixes bug <a href='https://bugs.kde.org/422260'>#422260</a></li>
<li>Fix KRunner positioning on X11 with High DPI and Qt scaling on Plasma. <a href='https://commits.kde.org/plasma-workspace/0f8c6c3a660d9778b04722f6a7319224205e3aa0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422578'>#422578</a></li>
<li>[lock screen] Stop timer when interacting with media controls. <a href='https://commits.kde.org/plasma-workspace/24fae256453aafa4aa5a9dc8b3c108df9bf00b56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422707'>#422707</a></li>
</ul>

</main>
<?php
	require('../aether/footer.php');