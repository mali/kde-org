---
aliases:
- ../announce-3.2beta1
custom_about: true
custom_contact: true
date: '2003-11-03'
description: The first beta of the upcoming KDE 3.2 release.
title: 'KDE 3.2 Beta 1: Say Hello To Rudi'
---

<div class="text-center">
<a href="/announcements/1-2-3/3.2-beta1/announce-3.2beta1.jpeg">
<img src="/announcements/1-2-3/3.2-beta1/announce-3.2beta1.jpeg" class="img-fluid" alt="Nov&eacute; Hrady" /></a> <br/>
Nov&eacute; Hrady, site of the 2003 KDE &quot;Kastle&quot; conference.
</div>
<br/>

<p>As summer waned in the Czech republic, over 100 participants from the KDE Project gathered at beautiful Nov&eacute; Hrady this year to collaborate on the next revision of <a href="http://www.kde.org">KDE</a>, the premier Open Source desktop. Against the backdrop of a medieval castle and modern University, workshops were held, ideas took shape and code was written.</p>

<p>With the new year fast approaching, beta testing of the upcoming KDE 3.2 release has begun. The <a href="http://www.kde.org">KDE Project</a> is excited to announce the immediate availability of &quot;Rudi&quot;, the first beta version of KDE 3.2. Sharing its name with the Kastle conference's host coordinator, Rudi represents the culmination of the KDE Project's efforts to date.</p>

<p>As no further features will be added between the release of Rudi and KDE 3.2, this is an excellent opportunity to preview the most advanced and powerful KDE yet. Packages may be downloaded from <a href="http://download.kde.org/unstable/3.1.93/">download.kde.org</a>. As of this writing there are sources and binaries for SuSE and Conectiva, but other distributions will follow with binaries and build scripts.</p>

<p>With its 25 page feature plan, nearly 10,000 bugs closed and over 2,000 user wishes processed a complete report on what one can expect from Rudi would be so long that by the time you finished reading it, there would probably be a more current KDE release to try out. The only way to truly appreciate this release is to experience it first hand, but here are a few of the highlights anyways...</p>

<h3>Getting To Know Rudi</h3>

<p>Being the third major release in the KDE3 series, version 3.2 delivers a more polished experience. Functionality that debuted in previous versions of KDE has been the focus of nearly 9 months of intense efforts by the KDE team of programmers, artists, writers and translators. This has led to a great number of improvements across the desktop.</p>

<p>For instance, Konqueror's ability to handle real world web pages has seen terrific improvement, and a powerful tabbed interface streamlines browsing tasks. While everyone can appreciate the impressive rendering speed and accuracy enhancements, advanced users will especially appreciate additions such as enhanced bookmarks and a Mozilla-compatible web sidebar. Browsing the web with Konqueror has never looked so good nor been so fast.</p>

<p>Another exciting web-related improvement in KDE 3.2 is the inclusion of an <a href="http://svg.kde.org/">advanced SVG viewer/player</a> which allows rich, interactive interfaces built using the open SVG standard to be used in Konqueror and other KDE applications. This emerging XML-based technology has wide industry support (e.g. <a href="http://www.adobe.com/svg/">Adobe</a>, <a href="http://www.w3.org/Graphics/SVG/">Mozilla</a>) and is a <a href="http://www.w3.org/Graphics/SVG/">World Wide Web Consortium</a> standard. KDE is the first desktop to provide such a complete and integrated implementation of this emerging technology.</p>

<p>The file management capabilities of Konqueror have also received attention. In addition to a variety of performance improvements, those same tabs that make web browsing more pleasant are also available for file management. Streamlined context menus allow quick access to specials actions, making tasks such as creating file archives or burning CDs simpler than ever.  Users can also <a href="http://developer.kde.org/documentation/tutorials/dot/servicemenus.html">create their own custom menu entries quickly</a> and easily.</p>

<p>Productivity improvements, such as in-line spell checking for web forms in Konqueror and email authoring in KMail, are immediately noticeable when using Rudi. Building on its cutting edge Desktop Sharing services, KDE now supports the Microsoft Windows RDP protocol. System administrators overseeing larger deployments of KDE will appreciate the maturation of the <a href="http://www.kde.org/areas/sysadmin/">KDE Kiosk</a> system and improved support for roaming users.</p>

<p> But KDE 3.2 is about more than just improving what already exists. Rudi showcases over 30 new applications and components that will appeal to a wide audience with a variety of interests: <a href="http://dot.kde.org/1055452455/">JuK</a> provides a simple yet powerful interface to your music collections. KPDF, which is based on xpdf, brings PDF file viewing in KDE to a whole new level. For the young (and young at heart) there are new games (KGoldRunner) and educational programs such as KDE Interactive Geometry (or KIG for short). KStars presents a remarkable planetarium experience with stunning views of its extensive star database and advanced features such as remote telescope control.</p>

<p>The innovative KWallet system makes managing personal information on the web safe and convenient. KWallet can store data such as completed web forms, passwords for web sites and instant messaging accounts in its encrypted, password protected storage for later retrieval. While only Konqueror and Kopete currently make use of the KWallet services, expect to see more applications using it in the future.</p>

<p>For software developers, <a href="http://uml.sourceforge.net/">Umbrello</a> opens up the power of UML to everyone and <a href="http://kcachegrind.sourceforge.net/">KCacheGrind</a> provides a means to profile applications. KDE 3.2 will ship with version 3 of the world class <a href="http://kdevelop.org">KDevelop</a> IDE, while web developers can look forward to the next version of <a href="http://quanta.sourceforge.net/">Quanta</a> which sports, among other things, a new HTML table designer and improved editor. KDE's DCOP (and graphical kdcop browser) along with tools such as <a href="http://developer.kde.org/documentation/tutorials/kdialog/t1.html">KDialog</a> are highly useful for shell scripting tasks.</p>

<p>Although not a part of the KDE desktop release, <a href="http://koffice.org/releases/1.3rc1-release.php">KOffice 1.3</a> will be released shortly before KDE 3.2. This maturing, integrated and light weight office suite is a highly valuable addition to KDE.</p>

<h3>Hardware and KDE 3.2</h3>

<p>KDE has made using computer peripherals on UNIX systems easier and more dynamic with <a href="http://www.kde.org/apps/kooka/">Kooka</a> for scanning and OCR, the enterprise-class <a href="http://printing.kde.org">KPrinter</a> printing system and camera:/ for digital photography. While each of these components have been improved, KDE 3.2 goes even further with several new hardware related additions.</p>

<p>As wireless technology has become a common part of the desktop, KDE has kept up. KWiFi provides a graphical interface to wireless networking, and with Rudi you can connect remote controls to virtually any KDE application: easily associate the &quot;play&quot; button with starting music playback, or the &quot;down&quot; button with checking for new email!</p>

<p>On the more traditional wired side of things, KMilo and a rewritten KHotKeys offer easy access to multimedia keyboards, user-defineable keyboard shortcuts and mouse gestures. While one new icon is rarely reason for much excitement, perhaps the icing on the cake in Rudi is KRandR which reduces changing screen resolutions or colour depths in newer versions of the X Window System to a few mouse clicks.  Finally, <b>no</b> configuration file editing and <b>no</b> desktop restarts!</p>

<h3>Using Rudi to Communicate With the World</h3>

<p>Groupware has emerged as perhaps one of the most important new business tools of the last 20 years. The award winning <a href="http://kmail.kde.org">KMail</a> and <a href="http://korganizer.org">KOrganizer</a> applications have both seen numerous speed and functionality improvements in their own rights, but a brand new way of using them together has been getting a lot of attention. At version 0.8, <a href="http://kontact.org/">Kontact</a> is a &quot;work in progress&quot; application that brings email, calendaring, contact management, notes and more together within a single common interface. Whether you prefer the simplicity and efficiency of stand alone applications or a more comprehensive groupware solution KDE 3.2 has something to offer.</p>

<p>Another communication technology that has taken the world by storm is instant messaging. With the plethora of messaging networks how does one stay in touch with all their IM buddies? Rudi answers with a single word: <a href="http://kopete.kde.org/">Kopete</a>! KDE 3.2 will be the first KDE release to include this multi-protocol instant messenger that features tight integration with KDE's addressbook, desktop panel and other applications.</p>

<p>Of course, to communicate with the world one must be able to speak many different languages. As an international project that caters to a diverse global audience, KDE has made this a priority. Of the over 60 languages KDE has been  translated into, 11 are already 90% or more complete in this beta release. Improvements in bi-directional text rendering for languages such as Hindi and support for various calendaring systems are some of the latest additions to KDE's world-class internationalization and localization efforts.</p>

<p>Language isn't the only barrier to desktop to usage, however. Some deal with special challenges such as blindness or motor control difficulties. The brand new <a href="http://accessibility.kde.org/">KDE Accessibility</a> package, while perhaps modest in its current incarnation, is an important and encouraging first step towards making KDE available to everyone. Working with the FSG Accessibility  Work Group and various experts in the accessibility field, KDE is on the path to being a fully accessible desktop system.</p>

<h3>A Good Looking 3.2</h3>

<p>While functionality is important, much is also expected from the look and feel of a modern desktop. Rudi helps push KDE further than it's ever been in this regard. Between a slimmed down and refined default Keramik look and a more comprehensive and improved set of icons KDE has never looked better. For those who live to tweak, there is a new style called Plastik and a new control panel for installing full color mouse cursor themes. Configurable drop shadows around text on desktop icons, improved panel backgrounds, new desktop wallpapers and many other look 'n feel improvements add to the sweetness of Rudi.</p>

<p>A more powerful and standards-compliant window manager debuts in this beta release, along with improved Xinerama and XDMCP support. Font management is another area where Rudi excels. Fonts can be installed using either the KDE Control Center or by entering &quot;fonts:/&quot; in Konqueror's location bar.</p>

<h3>Giving Something Back...</h3>

<p>The KDE project doesn't release betas <em>only</em> to whet the excitement of its users and vex the competition, betas are also a very important part of the development process. Those involved with creating KDE can test it in only so many different configurations, on so many different types of hardware and with only so many different needs and unique use cases. KDE relies in part on extensive testing by early adopters and the subsequent feedback to produce solid releases of featureful software. That's quite a task when the product contains millions of lines of code and dozens of individual applications. </p>

<p>The KDE Project is well aware that there are rough edges and bugs to be found in this beta release. You can help ensure a high-quality KDE 3.2 release by filing new bug reports and checking the validity of existing bug reports in the <a href="http://bugs.kde.org">KDE Bug Tracker</a> while using Rudi. Please keep in mind that no new features are going into KDE 3.2 at this point, so wishlist items aren't as useful as defect reports right now. With concise, detailed and accurate reports the KDE Project can better address outstanding needs.</p>

<p>If you are interested in finding out about other ways you can help out the KDE project, please visit the <a href="http://kde.org/support/">Support KDE</a> web site.</p>