---
aliases:
- ../announce-4.1-beta1
date: '2008-05-27'
title: KDE 4.1 Beta1 Release Announcement
---

<h3 align="center">
  Projeto KDE distribui o primeiro Beta do KDE 4.1
</h3>

<p align="justify">
  <strong>
A comunidade KDE anuncia a primeira versão Beta do KDE 4.1</strong>
</p>

<p align="justify">
27 de Maio de 2008 (A INTERNET).
O <a href="http://www.kde.org/">projeto KDE</a> tem orgulho de anunciar a
primeira versão beta do KDE 4.1.  Beta 1 é destinado a testadores, membros
 de comunidades e entusiastas para que possam identificar bugs e regressões, então o
4.1 pode substituir totalmente o KDE 3 para usuários finais.  KDE 4.1 beta 1 esta disponível como
pacotes binários para uma ampla faixa de plataformas, e também em forma de código-fonte.  KDE 
4.1 estará disponível na sua versão final em Julho de 2008.
</p>


<h4>
  <a id="changes">Pontos fortes do KDE 4.1 Beta 1</a>
</h4>

<p align="justify">
<ul>
    <li>Significativamente aumentada a funcionalidade e personalização do desktop
    </li>
    <li>Suite KDE Personal Information Management (PIM) portada para o KDE 4
    </li>
    <li>Muitas novas aplicações recém-portadas
    </li>
</ul>
</p>

<h4>
  O Plasma cresceu
</h4>
<p align="justify">
Plasma, o novo sistema inovador que cria os menus, painéis e desktop
que fazem uma Área de Trabalho, está amadurecendo rápidamente. Ele agora suporta múltiplos e
redimensionáveis painéis, permitindo aos usuários criarem seus desktops na flexibilidade que
tinhamos antes.  O menu lançador de aplicações, Kickoff, foi muito bem
polido com um novo visual limpo e muitas otimizações. Um revitalizado dialogo
"Rodar Comando" permite usuários avançados lançarem aplicações rápidamente, abrir
documentos e visitar sites.  Performance no gerenciamento de janelas compositado
provê melhor ergonomia e visuais, incluindo um Cover Switch 
alt-tab e o mandatório efeito de janelas wobbly.
</p>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-krunner.png">
	<img src="/announcements/4/4.1-beta1/plasma-krunner-small.png" class="img-fluid" alt="El nuevo KRunner (alt-F2)">
	</a> <br/>
	<em>El nuevo KRunner (alt-F2)</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-panelcontroller.png">
	<img src="/announcements/4/4.1-beta1/plasma-panelcontroller-small.png" class="img-fluid" alt="Vuelve la gestión de paneles">
	</a> <br/>
	<em>Vuelve la gestión de paneles</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-coverswitch.png">
	<img src="/announcements/4/4.1-beta1/kwin-coverswitch-small.png" class="img-fluid" alt="Efecto de Pase de Portadas de KWin">
	</a> <br/>
	<em>Efecto de Pase de Portadas de KWin</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly1.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly1-small.png" class="img-fluid" alt="Ventanas que ondean">
	</a> <br/>
	<em>Ventanas que ondean</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly2.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly2-small.png" class="img-fluid" alt="Más ventanas que ondean">
	</a> <br/>
	<em>Más ventanas que ondean</em>
</div>
<br/>

		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-krunner.png">
						<img width="310" height="235" src="announce_4.1-beta1/plasma-krunner-small.png" title="The new KRunner alt-F2 dialog" alt="The new KRunner alt-F2 dialog"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-panelcontroller.png">
						<img width="462" height="84" src="announce_4.1-beta1/plasma-panelcontroller-small.png" title="Panel management returns" alt="Panel management returns"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-coverswitch.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-coverswitch-small.png" title="KWin's Cover Switch effect" alt="KWin's Cover Switch effect"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly1.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly1-small.png" title="Windows that wobble" alt="Windows that wobble"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly2.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly2-small.png" title="More wobbling windows" alt="More wobbling windows"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
  Kontact Retorna
</h4>
<p align="justify">
Kontact, o Gerenciador de informações pessoais do KDE, e suas ferramentas associadas foi
portado para o KDE 4 e será lançado pela primeira vez com o KDE 4.1.
  Muitos recursos da versão Enterprise do KDE 3 foram incorporadas,
fazendo Kontact mais usável em configurações de negócios.  Melhorias incluem novos
componentes como um KTimeTracker e o tomador de notas KJots, um novo visual
 mais suave, melhor suporte para múltiplos calendários e timezones e uma manipulação
mais robusta de email.</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-calendar.png">
	<img src="/announcements/4/4.1-beta1/kontact-calendar-small.png" class="img-fluid" alt="Multiple calendars in use">
	</a> <br/>
	<em>Multiple calendars in use</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-kjots.png">
	<img src="/announcements/4/4.1-beta1/kontact-kjots-small.png" class="img-fluid" alt="The KJots outliner">
	</a> <br/>
	<em>The KJots outliner</em>
</div>
<br/>


<h4>
Aplicações do KDE 4 aumentam
</h4>
<p align="justify">
Pela comunidade KDE, muitas aplicações foram agora portadas para o KDE 4 ou
tiveram grandes aumentos de funcionalidade desde que o KDE 4 foi lançado. Dragon 
Player, o media player leve, faz o seu lançamento.  O tocador de CDs do KDE
retorna.   Um novo applet de impressão provê inigualável poder de impressão
e flexibilidade no Desktop Livre.  Konqueror ganha suporte para
suporte de sessões web, um modo Desfazer, um scrolling leve.  Um novo
modo de navegação de imagens incluindo uma interface em tela cheia chega ao Gwenview.
Dolphin, o gerenciador de arquivos, tem visualizações em abas, e muitos recursos apreciados
pelos usuários do KDE 3 incluindo Copiar Para, e uma arvore de diretórios melhorada.  Muitas aplicações,
incluindo o desktop e as aplicações do KDE Edu, agora estão provendo conteúdo novo
como ícones, temas, mapas, e materiais de lições via Get New Stuff,
que agora tem uma nova interface. Rede Zeroconf foi adicionado a diversos
jogos e utilidades, tirando a dor de configurar jogos e acesso remoto.
</p>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dolphin-treeview.png">
		<img src="/announcements/4/4.1-beta1/dolphin-treeview-small.png" class="img-fluid" alt="Dolphin's tree view">
		</a> <br/>
		<em>Dolphin's tree view</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dragonplayer.png">
		<img src="/announcements/4/4.1-beta1/dragonplayer-small.png" class="img-fluid" alt="Dragon media player">
		</a> <br/>
		<em>Dragon media player</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kdiamond.png">
		<img src="/announcements/4/4.1-beta1/games-kdiamond-small.png" class="img-fluid" alt="KDiamond puzzle game">
		</a> <br/>
		<em>KDiamond puzzle game</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kubrick.png">
		<img src="/announcements/4/4.1-beta1/games-kubrick-small.png" class="img-fluid" alt="The 80s on your desktop">
		</a> <br/>
		<em>The 80s on your desktop</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/konqueror.png">
		<img src="/announcements/4/4.1-beta1/konqueror-small.png" class="img-fluid" alt="Konqueror browser">
		</a> <br/>
		<em>Konqueror browser</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/marble-openstreetmap.png">
		<img src="/announcements/4/4.1-beta1/marble-openstreetmap-small.png" class="img-fluid" alt="Marble showing OpenStreetMaps">
		</a> <br/>
		<em>Marble showing OpenStreetMaps</em>
	</div>
	<br/>
<h4>
Melhorias pelos Frameworks
</h4>
<p align="justify">
Desenvolvedores estiveram ocupados melhorando as bibliotecas core do KDE e
a infraestrutura também.  KHTML recebeu um impulso na velocidade de
carregamento, enquanto o WebKit, a sua descendência, foi adicionado ao Plasma para permitir que Widgets
de Dashboard do OSX sejam usados no KDE.  O uso de Widgets no Canvas
faz o Plasma mais estável e leve.
As características do KDE com sua interface baseada em clique único ganha um novo mecanismo de seleção
que promete velocidade e acessibilidade.  Phonon, o framework de multimídia crossplataform,
ganha suporte a subtítulos e backends GStreamer, DirectShow 9 e QuickTime.
A camada de gerenciamento de rede foi extendida a suportar diversas versões
do NetworkManager.  E reconhecendo todos os valores da diversidade do Desktop Livre,
esforços para um desktop cross começaram, como suportar a notificação popup
e o formato de favoritos do freedesktop.org, então outras aplicações podem
rodar em uma única sessão do KDE 4.1.
</p>

<h4>
  KDE 4.1 Final Release
</h4>
<p align="justify">
KDE 4.1 is scheduled for final release on July 29, 2008.  This time based release falls six months after the release of KDE 4.0.
</p>

<h4>
  Pegue-o, rode-o, teste-o
</h4>
<p align="justify">
  Voluntários da comunidade e vendedores de Linux/UNIX distribuem gentilmente pacotes binários do KDE 4.0.80 (Beta 1) para a maioria
  das distribuições linux, e Mac OS X e Windows. Esteja avisado que esses pacotes não são considerados pronto para uso. Veja
  seu software de gerenciamento de pacotes ou para instruções especificar nos seguintes links:
</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> tem KDE 4.1beta1 no <em>experimental</em>.</li>
<li><em>Kubuntu</em> os pacotes estão no preparation.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>

<h4>
  Compilando o KDE 4.1 Beta 1 (4.0.80)
</h4>
<p align="justify">
  <a id="source_code"></a><em>Código-Fonte</em>.
  O código-fonte completo para o KDE 4.0.80 pode ser<a
  href="/info/4.0.80">livremente obtido</a>.
Instruções para compilação e instalação do KDE 4.0.80
  estão disponíveis na <a href="/info/4.0.80">Página de informações do KDE 4.0.80</a>, ou no <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>



